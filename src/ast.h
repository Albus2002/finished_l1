#pragma once
#include<iostream>
#include<cstring>
#include<memory>
#include "util.h"

using namespace std;

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST {
 public:
  // 用智能指针管理对象
  std::unique_ptr<BaseAST> func_def;
  void Dump() const override {
    std::cout << "fun ";
    func_def->Dump();
  }
};

// FuncDef 也是 BaseAST

class FuncDefAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> func_type;
  std::string ident;
  std::unique_ptr<BaseAST> block;
  void Dump() const override {
    if(ident == "main"){
        std::cout << "@"+ident+"(): ";
        func_type->Dump();
        std::cout << "{\n";
        block->Dump();
        cout << "}";
    }
    return;
  }
    
};

class FuncTypeAST : public BaseAST{
public:
 std::string func_type;
 void Dump() const override{
    if(func_type=="int"){
        cout<<"i32";
    }
    return;
 }

};

class BlockAST:public BaseAST{
public:
    std::unique_ptr<BaseAST> stmt;
    BlockAST() = default;
    void Dump() const override{
        std::cout<<"%entry:\n";
        stmt->Dump();
        return;
    }
};

class StmtAST : public BaseAST{
public:
    std::unique_ptr<BaseAST> number;
    StmtAST() = default;
    void Dump() const override{
        std::cout<< " ret ";
        number->Dump();
        return;
    }
};

class NumberAST : public BaseAST {
public:
    // -- INT_CONST
    int int_const = 0;
    NumberAST() = default;
    NumberAST(int _int_const) : int_const(_int_const) {}

    void Dump() const override {
        std::cout << int_const << std::endl;
        return;
    }
};

// ...

