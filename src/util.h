#pragma once
#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

// 所有 AST 的基类
class KIR{
    public:
    string s = "";
    bool end = 0;
    string type = "i32";

    KIR(){}
    KIR(string _s){this->s = _s;}
};

class BaseAST {
 public:
  virtual ~BaseAST() = default;
  virtual void Dump() const = 0;
};